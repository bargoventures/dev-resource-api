<?php

namespace Waqas\DevResourceApi;

use Illuminate\Support\ServiceProvider;

class ResourceCommandServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Waqas\DevResourceApi\Commands\AbstractResourceController',
        'Waqas\DevResourceApi\Commands\ApiResourceCommand',
        'Waqas\DevResourceApi\Commands\ApiResourceController',
        'Waqas\DevResourceApi\Commands\ApiResourceRepository',
        'Waqas\DevResourceApi\Commands\ApiRequestRules',
        'Waqas\DevResourceApi\Commands\ApiRequestFilters',
    ];
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }
}
